var xhttp = new XMLHttpRequest();

xhttp.open('GET', '/kohliStrikeRate_Data', true);
xhttp.send();

xhttp.onreadystatechange = function() {
  console.log(this.readyState);
}
xhttp.onload = function() {
  if (this.readyState == 4 && this.status == 200) {

    // Create the chart
    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Virat Kohli\'s strike rate across seasons'
      },
      subtitle: {
        text: 'Source: kaggle.com'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Strike Rate'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Strike Rate: <b>{point.y}</b>'
      },
      series: [{
        name: 'Strike Rates',
        data: JSON.parse(xhttp.response)
      }]
    });
  } else {
    alert('An Error Ocurred');
  }
}
xhttp.onerror = function() {
  alert('An error occured during transaction');
}
