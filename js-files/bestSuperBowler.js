var xhttp = new XMLHttpRequest();

xhttp.open('GET', '/bestSuperBowler_Data', true);
xhttp.send();

xhttp.onreadystatechange = function() {
  console.log(this.readyState);
}
xhttp.onload = function() {
  if (this.readyState == 4 && this.status == 200) {

    let obj = JSON.parse(xhttp.response);

    Highcharts.chart('container', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Best Economy of a Bowler in super overs'
      },
      subtitle: {
        text: 'Source: kaggle.com'
      },
      xAxis: {
        categories: 'Super over Economy',
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Economy (runs/over)',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: ' runs/over'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      // since the object only has 1 key-value pair, keys[0] and values[0] will return them
      series: [{
        name: Object.keys(obj)[0],
        data: [Object.values(obj)[0]]
      }]
    });
  } else {
    alert('An Error Ocurred');
  }
}
xhttp.onerror = function() {
  alert('An error occured during transaction');
}
