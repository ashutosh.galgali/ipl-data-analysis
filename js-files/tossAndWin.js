var xhttp = new XMLHttpRequest();

xhttp.open('GET', '/tossAndWin_Data', true);
xhttp.send();

xhttp.onreadystatechange = function() {
  console.log(this.readyState);
}
xhttp.onload = function() {
  if (this.readyState == 4 && this.status == 200) {

    let obj = JSON.parse(xhttp.response);
    let teams = Object.keys(obj);
    let seriesArray = [];

    teams.map(function(teamNames){
      seriesArray.push([teamNames, obj[teamNames]]);
    });

    // Create the chart
    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Teams that won both, the toss and the game'
      },
      subtitle: {
        text: 'Source: kaggle.com'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -50,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total Wins'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Tosses and Matches: <b>{point.y}</b>'
      },
      series: [{
        name: 'Tosses and Matches Won',
        data: seriesArray
      }]
    });
  } else {
    alert('An Error Ocurred');
  }
}
xhttp.onerror = function() {
  alert('An error occured during transaction');
}
