var xhttp = new XMLHttpRequest();

xhttp.open('GET', '/teamWinsPerYear_Data', true);
xhttp.send();

xhttp.onreadystatechange = function() {
  console.log(this.readyState);
}
xhttp.onload = function() {
  if (this.readyState == 4 && this.status == 200) {

    let obj = JSON.parse(xhttp.response);


    let teams = Object.keys(obj);
    let scores = Object.values(obj);
    let categoryArray = [];

    for (let i = 2008; i <= 2017; i++) {
      categoryArray.push(i);
    }
    /* seriesArray will contain objects with two key-value pairs
    [{name : team_name, data : [wins_in_season1, wins_in_season2, ....]}]
    for example, [{'name' : 'Sunrisers Hydrabad', 'data': [0, 7, 8, ...]}, {'name' : 'RCB' : [0, 0, 1, ...]}]*/
    let seriesArray = [];

    //for loop used here to essentially iterate over the object 'obj'
    for (let i = 0; i < teams.length; i++) {
      let pushObj = {};
      pushObj['name'] = teams[i];
      pushObj['data'] = scores[i];

      seriesArray.push(pushObj);
    }


    // Create the chart
    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Team-wise, season-wise victories'
      },
      subtitle: {
        text: 'kaggle.com'
      },
      xAxis: {
        categories: categoryArray,
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of victories'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y} wins</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: seriesArray
    });
  } else {
    alert('An Error Ocurred');
  }
}
xhttp.onerror = function() {
  alert('An error occured during transaction');
}
