var xhttp = new XMLHttpRequest();

xhttp.open('GET', '/nemesisBowler_Data', true);
xhttp.send();

xhttp.onreadystatechange = function() {
  console.log(this.readyState);
}
xhttp.onload = function() {
  if (this.readyState == 4 && this.status == 200) {

    let obj = JSON.parse(xhttp.response);

    Highcharts.chart('container', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Maximum dismissals of a player by the same bowler'
      },
      subtitle: {
        text: 'Source: kaggle.com'
      },
      xAxis: {
        categories: [obj['Dismissed Batsman']],
        title: {
          text: 'Dismissed Batsman'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Dismissals (across all seasons)',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: ' dismissals'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: obj['Dismissing Bowler'],
        data: [obj['Number of dismissals']]
      }]
    });
  } else {
    alert('An Error Ocurred');
  }
}
xhttp.onerror = function() {
  alert('An error occured during transaction');
}
