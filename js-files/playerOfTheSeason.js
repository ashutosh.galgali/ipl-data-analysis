var xhttp = new XMLHttpRequest();

xhttp.open('GET', '/playerOfTheSeason_Data', true);
xhttp.send();

xhttp.onreadystatechange = function() {
  console.log(this.readyState);
}
xhttp.onload = function() {
  if (this.readyState == 4 && this.status == 200) {

    let obj = JSON.parse(xhttp.response);

    /*obj is an object containing data in the following format:
    {
    'season' : ['winner_of_most_MVPs', number_of_mvps_won]
  } */

    let seasons = Object.keys(obj).sort(function(a, b) {
      return 1 - b;
    });

    /* seriesArray will contain [[season (winner_of_most_MVPs), number_of_mvps_won]]
    for example, [[2008(BA Stokes), 3], [2009(V Kohli), 5], ... ] */
    let seriesArray = [];

    seasons.map(function(keys) {
      seriesArray.push([keys + ' (' + obj[keys][0] + ')', obj[keys][1]]);
    });

    // Create the chart
    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Players with most MVP awards for each season'
      },
      subtitle: {
        text: 'Source: kaggle.com'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'MVP awards'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'MVP awards: <b>{point.y}</b>'
      },
      series: [{
        name: 'MVP winners',
        data: seriesArray
      }]
    });
  } else {
    alert('An Error Ocurred');
  }
}
xhttp.onerror = function() {
  alert('An error occured during transaction');
}
