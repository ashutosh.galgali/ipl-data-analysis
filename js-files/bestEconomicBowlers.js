var xhttp = new XMLHttpRequest();

xhttp.open('GET', '/bestEconomicBowlers_Data', true);
xhttp.send();

xhttp.onreadystatechange = function() {
  console.log(this.readyState);
}
xhttp.onload = function() {
  if (this.readyState == 4 && this.status == 200) {

    // Create the chart
    Highcharts.chart('container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Most economic bowlers across seasons'
      },
      subtitle: {
        text: 'Source: kaggle.com'
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Economies (runs/over)'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: 'Economy: <b>{point.y}</b>'
      },
      series: [{
        name: 'Economies',
        data: JSON.parse(xhttp.response)
      }]
    });
  } else {
    alert('An Error Ocurred');
    document.querySelector('body').innerHTML = this.response;
  }
}
xhttp.onerror = function() {
  alert('An error occured during transaction');
  document.querySelector('body').innerHTML = "<h1 color = 'red'>An error occured while accessing this page</h1> <a href = '/'>Take me back to safety</a>"
}
