IPL data analysis:

This project takes data from kaggle.com and extracts statistics from it.
It shows the use of module extracting/importing to perform tasks while separating the entry point from the functions.

It also has charts to help visualize the data.

`How to use:``
To run this project, you need to run /server/index.js first.
Once the file has been run, you can then start the server by
running /server/server.js
The port is set to 7000.
