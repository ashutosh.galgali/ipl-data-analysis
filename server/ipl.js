var fs = require('fs')

// Creating a class and constructor to hold data sent by child module
module.exports = class Stats {
  constructor(jsonObj) {
    this.allData = jsonObj;
  }

  // calculate total matches played per year, uses data from matches.csv
  yearlyMatches() {
    var matches = {};
    var seasons = this.allData.map(function(year) {
      if (matches[year.season] != undefined) {
        matches[year.season] += 1;
      } else {
        matches[year.season] = 1;
      }
    });
    let keys = Object.keys(matches);
    let vals = Object.values(matches);

    let outputArray = [];
    //Array of arrays is the required format for running Highcharts. The output is no different than the Object
    //i.e. it contains [year, matches-played]
    for (let i = 0; i < keys.length; i++) {
      outputArray.push([keys[i], vals[i]]);
    }
    return outputArray;
  }

  // calculate total wins per team per year, uses matches.csv
  teamScores() {
    var scores = {};
    var teams = {};
    this.allData.map(function(team) {
      if (teams[team.team1] === undefined) {
        teams[team.team1] = [];
      }
      if (teams[team.team2] === undefined) {
        teams[team.team2] = [];
      }
    });

    this.allData.map(function(year) {
      if (scores[year.season] === undefined) {
        scores[year.season] = {};
      }
    });
    let teamArray = Object.keys(teams)
    let seasons = Object.keys(scores).sort(function(a, b) {
      return a - b
    });

    var teamScores = this.allData.map(function(winner) {
      if (scores[winner.season][winner.winner] != undefined) {
        scores[winner.season][winner.winner] += 1;
      } else {
        scores[winner.season][winner.winner] = 1;
      }
    });

    for (let year = 0; year < seasons.length; year++) {
      for (let org = 0; org < teamArray.length; org++) {
        if (scores[seasons[year]][teamArray[org]] === undefined) {
          teams[teamArray[org]].push(0);
        } else {
          teams[teamArray[org]].push(scores[seasons[year]][teamArray[org]]);
        }
      }
    }
    /*Additional File that contains better formatted output more easily usable with Highcharts
    It contains {team_name: [victories_in_season_1, victories_in_season_2,...]}
    It assigns 0 if the that team has not played in that season*/
    fs.writeFile('../output/teamWinsPerYear.json', JSON.stringify(teams), function(err, data) {
      if (err) throw err;
      console.log('file saved');
    })

    return scores;
  }

  // Extract match IDs of specific years
  matchIdValues(year) {

    var matchIDs = this.allData.filter((seasons) => {
      return seasons.season == year;
    });
    /* matchIDs is an array that holds [starting id, ending id] of the year
    for example, if the year is 2017, the array will be [1,79] */
    return [parseInt(matchIDs[0].id), parseInt(matchIDs[matchIDs.length - 1].id)];
  }

  // Calculate extra runs conceeeded by each team for a given year
  extraRuns(start, end) {
    var extras = {};
    this.allData.map(function(team) {
      if (parseInt(team.match_id) >= start && team.match_id <= end) {
        if (extras[team.bowling_team] != undefined) {
          extras[team.bowling_team] += parseInt(team.extra_runs);
        } else {
          extras[team.bowling_team] = parseInt(team.extra_runs);
        }
      }
    });
    let outputArray = [];
    for (let i = 0; i < Object.keys(extras).length; i++) {
      outputArray.push([Object.keys(extras)[i], Object.values(extras)[i]]);
    }
    return outputArray;
  }

  //find the 10 most economic bowlers for a given season
  economy(start, end) {
    var balls = {};
    var runs = {};
    var economies = {};
    var bestEco = {};
    var bowlers = [];
    var sortedEconomies = [];

    this.allData.map(function(player) {

      //this block checks if a bowler has already been evaluated. If yes, it increments the balls bowled and runs given away
      if (parseInt(player.match_id) >= start && parseInt(player.match_id) <= end) {
        if (balls[player.bowler]) {
          if (player.wide_runs == 0 && player.noball_runs == 0) {
            balls[player.bowler] += 1;
          }
          runs[player.bowler] += parseInt(player.batsman_runs) + parseInt(player.wide_runs) + parseInt(player.noball_runs);
        } else {
          // this block is executed when a bowler is not previously evaluated.
          if (player.wide_runs == 0 && player.noball_runs == 0) {
            balls[player.bowler] = 1;
          } else {
            balls[player.bowler] = 0;
          }
          runs[player.bowler] = parseInt(player.batsman_runs) + parseInt(player.wide_runs) + parseInt(player.noball_runs)
          bowlers.push(player.bowler);
        }
      }
    });

    bowlers.map(function(ecos) {
      economies[ecos] = runs[ecos] / (balls[ecos] / 6);
    });

    sortedEconomies = Object.values(economies).sort(function(a, b) {
      return a - b
    });

    /*check if the value 'leastVal' exists in the Object 'ecos'.
    If true, return the corresponding key */
    function bestBowler(ecos, leastVal) {
      return Object.keys(ecos).find(key => economies[key] === leastVal);
    }

    //ten loops since 10 best bowlers are required
    for (let i = 0; i < 10; i++) {
      bestEco[bestBowler(economies, sortedEconomies[i])] = sortedEconomies[i];
    }

    //bestEco is an object that contains {bowler-name: economy} pairs of the 10 best bowlers
    //i.e. bowlers with the lowest economies

    let outputArray = [];
    for (let i = 0; i < Object.keys(bestEco).length; i++) {
      outputArray.push([Object.keys(bestEco)[i], Object.values(bestEco)[i]]);
    }

    return outputArray;
  }


  // calculate how often a team won both the toss and the game
  tossAndWin() {
    var tossWin = {};

    this.allData.map(function(tosses) {
      if (tosses.toss_winner === tosses.winner) {
        if (tossWin[tosses.winner] != undefined) {
          tossWin[tosses.winner] += 1;
        } else {
          tossWin[tosses.winner] = 1;
        }
      }
    });

    // tossWin contains {team-name: win-count} pairs
    return tossWin;
  }

  // find the player with most Player of the Match awards in each season
  playerOfTheSeason(startingYear, endingYear) {
    var playerOfSeason = {};
    var playerOfMatch = {};
    var playerNames = [];

    this.allData.map(function(year) {
      if (playerOfMatch[year.season] === undefined) {
        playerOfMatch[year.season] = {};
      }
    });

    this.allData.map(function(names) {
      if (playerOfMatch[names.season][names.player_of_match]) {
        playerOfMatch[names.season][names.player_of_match] += 1;
      } else {
        playerOfMatch[names.season][names.player_of_match] = 1;
        playerNames.push(names.player_of_match);
      }
    });


    for (var i = startingYear; i <= endingYear; i++) {
      var maxMVP = 0;
      var pos = '';

      playerNames.map(function(names) {
        if (playerOfMatch[i][names] > maxMVP) {
          maxMVP = playerOfMatch[i][names];
          pos = names;
        }
      });
      playerOfSeason[i] = [pos, maxMVP];
      maxMVP = 0;
      pos = '';
    }
    return playerOfSeason;
  }

  // calculate Virat Kohli's strike rate
  //start and end years represent the first and last season of available data
  viratKohli(listOfIds, startYear, endYear) {
    var vkRate = {}
    var vkBalls = {}
    var start = [];
    var end = [];
    for (var year = startYear; year <= endYear; year++) {
      /* listOfIds contains {season:[starting_match_id, ending_match_id]} pairs for each season
      for example, if the season in question is 2017, listOfIds would be {2017:[1,79]} */
      start.push(listOfIds[year][0]);
      end.push(listOfIds[year][1]);
      vkRate[year] = 0;
      vkBalls[year] = 0;
    }

    this.allData.map(function(vk) {
      if (vk.batsman === 'V Kohli') {
        for (var year = startYear; year <= endYear; year++) {
          if (parseInt(vk.match_id) >= start[year - startYear] && parseInt(vk.match_id) <= end[year - startYear]) {
            vkRate[year] += parseInt(vk.batsman_runs);
            if (vk.wide_runs == 0 && vk.noball_runs == 0) {
              vkBalls[year] += 1;
            }
          }
        }
      }
    });
    for (var year = startYear; year <= endYear; year++) {
      vkRate[year] = ((vkRate[year] / vkBalls[year]) * 100).toFixed(2);
    }
    let outputArray = [];
    for (let i = 0; i < Object.keys(vkRate).length; i++) {
      outputArray.push([Object.keys(vkRate)[i], parseFloat(Object.values(vkRate)[i])]);
    }
    return outputArray;
  }

  // Calculate the highest number of dismissals by one player against another
  dismissals() {
    var dismissedBatsmen = [];
    var dismissBowlers = [];
    var dismissChecker = {};
    var dismissalPairs = {};
    var maxDismissals = {
      'Dismissed Batsman': '',
      'Dismissing Bowler': '',
      'Number of dismissals': 0
    };
    var maxVal = 0;
    var posOfBatsman = 0;
    var posOfBowler = 0;

    this.allData.map(function(dismisser) {
      if (dismisser.player_dismissed != '' && dismisser.dismissal_kind != 'run out') {
        if (dismissalPairs[dismisser.player_dismissed]) {
          //this block runs if the current dismissed batsman has previously been evaluated
          if (dismissalPairs[dismisser.player_dismissed][dismisser.bowler]) {
            dismissalPairs[dismisser.player_dismissed][dismisser.bowler] += 1;
          } else {
            dismissalPairs[dismisser.player_dismissed][dismisser.bowler] = 1;
          }
        } else {
          //this block creates an initial evaluation for dismissed batsmen being evaluated for the first time
          dismissalPairs[dismisser.player_dismissed] = {};
          dismissalPairs[dismisser.player_dismissed][dismisser.bowler] = 1;
          dismissedBatsmen.push(dismisser.player_dismissed);
        }
      }
    });

    this.allData.map(function(checker) {
      if (checker.player_dismissed != '') {
        if (dismissChecker[checker.bowler]) {} else {
          dismissChecker[checker.bowler] = 1;
          dismissBowlers.push(checker.bowler);
        }
      }
    })

    for (var batsmanPosition = 0; batsmanPosition < dismissedBatsmen.length; batsmanPosition++) {
      for (var bowlerPosition = 0; bowlerPosition < dismissBowlers.length; bowlerPosition++) {
        if (dismissalPairs[dismissedBatsmen[batsmanPosition]][dismissBowlers[bowlerPosition]] > maxVal) {
          maxVal = parseInt(dismissalPairs[dismissedBatsmen[batsmanPosition]][dismissBowlers[bowlerPosition]]);
          posOfBatsman = batsmanPosition;
          posOfBowler = bowlerPosition;
        }
      }
    }
    maxDismissals['Dismissed Batsman'] = dismissedBatsmen[posOfBatsman];
    maxDismissals['Dismissing Bowler'] = dismissBowlers[posOfBowler];
    maxDismissals['Number of dismissals'] = dismissalPairs[maxDismissals['Dismissed Batsman']][maxDismissals['Dismissing Bowler']];

    return maxDismissals
  }

  // Calculate the most economic bowler in super overs
  superEconomy() {
    var bowlers = [];
    var supEco = {}; //stands for super-over-economy
    var runs = {};
    var balls = {};
    var eco = 10 ** 5 // Initial value set to be much higher than economies can practically get
    var pos = '';
    var output = {};

    this.allData.map(function(sup) {
      if (sup.is_super_over == '1') {
        if (runs[sup.bowler] != undefined) {
          runs[sup.bowler] += parseInt(sup.batsman_runs) + parseInt(sup.wide_runs) + parseInt(sup.noball_runs);
          if (sup.wide_runs == 0 && sup.noball_runs == 0) {
            balls[sup.bowler] += 1;
          }
        } else {
          runs[sup.bowler] = parseInt(sup.batsman_runs) + parseInt(sup.wide_runs) + parseInt(sup.noball_runs);
          if (sup.wide_runs == 0 && sup.noball_runs == 0) {
            balls[sup.bowler] = 1;
          } else {
            balls[sup.bowler] = 0;
          }
          bowlers.push(sup.bowler);
        }
      }
    });

    var overs = bowlers.map(function(ecos) {
      balls[ecos] /= 6;
      supEco[ecos] = runs[ecos] / balls[ecos];
    });

    for (var i = 0; i < bowlers.length; i++) {
      if (supEco[bowlers[i]] < eco) {
        eco = supEco[bowlers[i]];
        pos = i;
      }
    }
    output[bowlers[pos]] = eco;

    //output contains a single key:value pair, {bowler: economy}
    return output;
  }
}
