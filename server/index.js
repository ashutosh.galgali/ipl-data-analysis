//required modules:
var fs = require('fs');
const IplStats = require('./ipl.js');
const csvFilePath = '../data/matches.csv';
const csvFilePath2 = '../data/deliveries.csv';
const csv = require('csvtojson');

// converting csv files to .json
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    //creating a constant to import from ipl.js which contains all functions
    const iplStats = new IplStats(jsonObj);

    //Matches Played per year
    fs.writeFile('../output/matchesPerYear.json', JSON.stringify(iplStats.yearlyMatches()), function(err, file) {
      if (err) throw err;
      console.log('File Saved.');
    });

    // Calculate wins per team, per year
    fs.writeFile('../output/teamScoresPerYear.json', JSON.stringify(iplStats.teamScores()), function(err, file) {
      if (err) throw err;
      console.log('File Saved.');
    });

    //determine how often a team won both the toss and the game
    fs.writeFile('../output/tossAndWin.json', JSON.stringify(iplStats.tossAndWin()), function(err, file) {
      if (err) throw err;
      console.log('File Saved.');
    });

    /* storing the IDs for specific years for later use
    matchIdValues(year) returns an array containing [starting match id, ending match id] of that year
    i.e. matchIdValues(2009)[0] = id of the first match played in 2009
    and matchIdValues(2009)[1] = id of the last match played in 2009 */
    var start2016 = iplStats.matchIdValues(2016)[0];
    var end2016 = iplStats.matchIdValues(2016)[1];

    var start2015 = iplStats.matchIdValues(2015)[0];
    var end2015 = iplStats.matchIdValues(2015)[1];

    // storing all IDs for future use
    var listOfIds = {}

    const startYear = 2008; //The very first season of IPL.

    let allYears = jsonObj.map((year) => {
      return year.season;
    });

    //The last value in the sorted arry is the most recent season and is hence stored here
    let endYear = allYears.sort(function(a, b) {
      return a - b
    })[allYears.length - 1];

    for (i = startYear; i <= endYear; i++) {
      listOfIds[i] = iplStats.matchIdValues(i);
    }

    // determine the player of the season
    fs.writeFile('../output/playerOfTheSeason.json', JSON.stringify(iplStats.playerOfTheSeason(startYear, endYear)), function(err, file) {
      if (err) throw err;
      console.log('File Saved.');
    });


    // new call to the CSVTOJSON package to parse second data file

    csv()
      .fromFile(csvFilePath2)
      .then((jsonObj) => {
        const iplStats2 = new IplStats(jsonObj);

        // Calculate the extra runs conceeded by each team
        fs.writeFile('../output/extrasPerTeam2016.json', JSON.stringify(iplStats2.extraRuns(start2016, end2016)), function(err, file) {
          if (err) throw err;
          console.log('File Saved.');
        });

        //determine the 10 most economical bowlers
        fs.writeFile('../output/bestEconomicBowlers.json', JSON.stringify(iplStats2.economy(start2015, end2015)), function(err, file) {
          if (err) throw err;
          console.log('File Saved.');
        });

        // V Kohli's season-wise strike rates
        fs.writeFile('../output/kohliStrikeRate.json', JSON.stringify(iplStats2.viratKohli(listOfIds, startYear, endYear)), function(err, file) {
          if (err) throw err;
          console.log('File Saved.');
        });

        // Find who dismissed whom the most
        fs.writeFile('../output/nemesisBowler.json', JSON.stringify(iplStats2.dismissals()), function(err, file) {
          if (err) throw err;
          console.log('File Saved.');
        });

        // Calculate super-over economy
        fs.writeFile('../output/bestSuperBowler.json', JSON.stringify(iplStats2.superEconomy()), function(err, file) {
          if (err) throw err;
          console.log('File Saved.');
        });
      });
  });
