var http = require('http');
var fs = require('fs');

http.createServer((req, res) => {
  if (req.url === '/') {
    fs.readFile('./index.html', 'utf-8', (err, data) => {
      if (err) {
        fs.readFile('./error.html', 'utf-8', (err, data) => {
          if (err) throw err;
          else {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            })
            res.write(data);
            res.end();
          }
        });
      } else {
        res.writeHead(200, {
          'Content-Type': 'text/html'
        })
        res.write(data);
        res.end();
      }
    });
  } else if (req.url.substr(req.url.length - 4) === 'Data') {
    let url = req.url.substr(0, req.url.length - 5);

    fs.readFile('../output' + url + '.json', 'utf-8', (err, data) => {
      if (err) {
        fs.readFile('./error.html', 'utf-8', (err, data) => {
          if (err) throw err;
          else {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            })
            res.write(data);
            res.end();
          }
        });
      } else {
        res.writeHead(200, {
          'Content-Type': 'application/json'
        })
        res.write(data);
        res.end();
      }
    });
  } else if (req.url.substr(req.url.length - 2) === 'js') {
    let url = req.url.substr(0, req.url.length - 3);

    fs.readFile('../js-files' + url + '.js', 'utf-8', (err, data) => {
      if (err) {
        fs.readFile('./error.html', 'utf-8', (err, data) => {
          if (err) throw err;
          else {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            })
            res.write(data);
            res.end();
          }
        });
      } else {
        res.writeHead(200, {
          'Content-Type': 'text/html'
        })
        res.write(data);
        res.end();
      }
    });
  } else if (req.url.substr(req.url.length - 3) === 'css') {
    let url = req.url.substr(0, req.url.length - 4);
    console.log(url);
    fs.readFile('../css-files' + url + '.css', 'utf-8', (err, data) => {
      if (err) {
        fs.readFile('./error.html', 'utf-8', (err, data) => {
          if (err) throw err;
          else {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            })
            res.write(data);
            res.end();
          }
        });
      } else {
        res.writeHead(200, {
          'Content-Type': 'text/css'
        })
        res.write(data);
        res.end();
      }
    });
  } else {
    fs.readFile('../html-files' + req.url + '.html', 'utf-8', (err, data) => {
      if (err) {
        fs.readFile('./error.html', 'utf-8', (err, data) => {
          if (err) throw err;
          else {
            res.writeHead(404, {
              'Content-Type': 'text/html'
            })
            res.write(data);
            res.end();
          }
        });
      } else {
        res.writeHead(200, {
          'Content-Type': 'text/html'
        })
        res.write(data);
        res.end();
      }
    });
  }
}).listen(7000, () => {
  console.log('server started');
});
